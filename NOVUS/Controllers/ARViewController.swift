//
//  ARViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/2/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import SceneKit
import UIKit
import ARKit
import DeviceKit
import SceneKit.ModelIO



class ARViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet  var sceneView: ARSCNView!
    @IBOutlet weak var NoAR: UILabel!
    
    var CellSelected = ""
    var CellDirectory = ""
    var device = Device.current.description
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //MARK: Device detection
        switch device {
        
        case "iPad Air":
            
            sceneView.isHidden = true
            
        case "iPad (6th generation)":
            
            sceneView.isHidden = false
            sceneView.delegate = self
            NoAR.isHidden = true
            //var scene: SCNScene!
            let scene = SCNScene(named: CellSelected, inDirectory: CellDirectory)!
            sceneView.autoenablesDefaultLighting = true
            sceneView.scene = scene
        default:
            
            break;
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    //Initialize  AR Session
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    @IBAction func closeAction(_ sender: Any) {
        switch CellSelected {
        case "AnmCell.usdz":
           dismiss(animated: true
            , completion: nil)
        case "VegCell.scn":
            dismiss(animated: true
                , completion: nil)
        case "BactCell.scn":
            dismiss(animated: true
                , completion: nil)
        default:
            break
        }
    }

}
