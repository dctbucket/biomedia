//
//  AnimFX.swift
//  NOVUS
//
//  Created by DCT on 6/6/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func disabled(){
        self.setBackgroundImage(UIImage(named: "ARblocked"), for: UIControl.State.disabled)
        self.isEnabled = false
    }
    
    func enabled(){
        self.isEnabled = true
        self.alpha = 1
       
    }
}
extension UIImageView {
    
    func pulsate() {
        self.alpha = 1
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.6
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 1
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
    }
    
    func applymotionfx(for view:UIImageView, magnitude:Float) {
        
        let xMotion = UIInterpolatingMotionEffect(keyPath:"center.x", type: .tiltAlongHorizontalAxis )
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        let yMotion = UIInterpolatingMotionEffect(keyPath:"center.y", type: .tiltAlongVerticalAxis )
        yMotion.minimumRelativeValue = magnitude
        yMotion.maximumRelativeValue = -magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        view.addMotionEffect(group)
    }
}

class backgroundImage {
    static func bg(_ view: UIView, name: String) {

        // Se define el tipo de objeto
        let backgroundImage = UIImageView()
        //Carga la imagen de fondo
        backgroundImage.image = UIImage(named: name)
        // Se fijan los contrains de la imagen a los valores de la vista
        view.addSubview(backgroundImage)
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        
        view.sendSubviewToBack(backgroundImage)
    }
    
//    Parallax Effect for case views
    }



    
    
    
    
    
    
   

