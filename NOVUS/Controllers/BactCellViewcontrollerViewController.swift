//
//  BactCellViewcontrollerViewController.swift
//  NOVUS
//
//  Created by DCT on 07/11/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit
import MobileCoreServices
import Foundation
import QuickLook

class BactCellViewcontrollerViewController: UIViewController, UIDragInteractionDelegate, UIDropInteractionDelegate, QLPreviewControllerDelegate, QLPreviewControllerDataSource {

    //MARK: - Properties
      
      var originButton: UIButton?
      var destinationButton: UIButton?
      
      @IBOutlet weak var evalButton: UIButton!
      @IBOutlet weak var ARButton: UIButton!
      @IBOutlet weak var termTitle: UILabel!
      @IBOutlet weak var termDescription: UITextView!
      @IBOutlet var parts: [UIImageView]!
      @IBOutlet var options: [UIButton]!
      @IBOutlet weak var MedalView: UIImageView!
    
      var counter = 0
      var cellType = "BactCell.scn"
      var cellDirectory = "/BactCell"
      
      var medalRepository: RealSession?
      let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      
      var medal = ""
      var intentos: Int = 1
      
      let terms: [String] = ["Plásmidos", "Citoplasma", "ADN y Nucléolo", "Fimbria", "M. Plasmática", "Pared celular", "Glicocálix", "Flagelo", "Pilus", "Ribosoma"]
      
      let descriptions: [String: String] = ["Plásmidos": "Contiene información que puede proteger a la célula de antibióticos.", "Citoplasma": "Espacio en donde se encuentran los organelos.", "ADN y Nucléolo": "Micromolécula que codifica información unida a proteínas.", "Fimbria": "Microvellosidad que permite la movilidad en una superficie.", "Membrana plasmática": "Bicapa lipídica que permite aislar a la célula. ", "Pared celular": "Estructura que protege a la bacteria de la entrada de sustancias; gram positiva.", "Glicocálix": "Permite que una bacteria se pueda pegar a una superficie.", "Flagelo": "Permite la movilidad celular en un medio líquido. ", "Pilus": "Permite el intercambio de información para su protección. ", "Ribosoma": "Encargado de la producción de proteínas, tiene dos subunidades, y está formado por ARNr."]
      
      // MARK: -
      
      override func viewDidLoad() {
          super.viewDidLoad()
          backgroundImage.bg(view, name: "bactBG")
          
          let dragInteraction = UIDragInteraction(delegate: self)
          let dropInteraction = UIDropInteraction(delegate: self)
          dragInteraction.isEnabled = true
          
          view.addInteraction(dropInteraction)
          view.addInteraction(dragInteraction)
          
          ARButton.disabled()
          medalRepository = RealSession(context)
          evalButton.isEnabled = false
          
          
      }
      
      // MARK: - Activity Methods
      
      @IBAction func setDesc(_ sender: UIButton) {
         hintName(sender)
      }
    func hintName(_ sender: UIButton){
          for description in descriptions {
              if sender.accessibilityLabel ?? String() == description.key {
                  termTitle.text = description.key
                  termDescription.text = " "
              }
            termTitle.isHidden = false
            termDescription.isHidden = true
          }
      }
    func hintDesc(_ sender: UIButton){
             for description in descriptions {
                 if sender.accessibilityLabel ?? String() == description.key {
                    termDescription.text = description.value
                    termTitle.text = " "
                 }
                termDescription.isHidden = false
                termTitle.isHidden = true
             }
         }
      
      func getCellPart(senderTag: Int) -> UIImageView {
          var selectedPart = UIImageView()
          
          parts.forEach { part in
              if  part.tag == senderTag {
                  selectedPart = part
              }
            part.alpha = 0.5
          }
          return selectedPart
      }
      
      
      @IBAction func optionSelect(sender: UIButton){
        getCellPart(senderTag: sender.tag).pulsate()
        hintDesc(sender)
        
        print(sender.tag)
        //print(sender.accessibilityLabel)
      }
      
      
      @IBAction func dismissView(_ sender: Any) {
          dismiss(animated: true, completion: nil)
      }
      
     
      
      func evalButtonCheck() {
          var fail = false
          for number in 0..<options.count {
              let button = options[number]
              print(number)
              if button.currentTitle == nil || button.currentTitle == "" {
                  print ("incomplete")
                  fail = true
                  break
              } else {
                  print ("passes")
              }
          }
          if fail == false {
              evalButton.isEnabled = true
          }
      }
      
      // MARK: - Drag and Drop methods
      
      func hasTextButton(location: CGPoint) -> UIButton? {
          if let button = self.view.hitTest(location, with: nil) as? UIButton {
              if let title = button.currentTitle {
                  if title == "" {
                      return nil
                  }
                  return button
              }
          }
          return nil
      }
      
      func buttonUpdate(location: CGPoint, newText: String) {
          if let button = self.view.hitTest(location, with: nil) as? UIButton {
              if button.currentTitle == nil || button.currentTitle == "" {
                  originButton?.setTitle("", for: .normal)
                                      originButton?.setBackgroundImage(UIImage(named: "Empty"), for: .normal)
                  print(originButton!.tag)
                  if originButton!.tag > 0 {
                      originButton?.setTitle("", for: .normal)
                      print("persistent label")
                  } else {
                      originButton?.removeFromSuperview()
                  }
                  button.setTitle(newText, for: .normal)
                  button.setBackgroundImage(UIImage(named: "BactBtn"), for: .normal)
                  
              } else {
                  print("texted label")
                  let tempText = originButton!.titleLabel?.text
                  originButton?.setTitle(button.titleLabel?.text, for: .normal)
                  button.setTitle(tempText, for: .normal)
                  
              }
              buttonAnswer(button)
              
          }
      }
      
      func buttonAnswer(_ button: UIButton) {
          if button.tag < 0 {
              //do nothing
          } else {
              print("tag is \(button.tag-1) with answer \(terms[button.tag-1])")
          }
      }
      
      func completeCell(){
          parts.forEach{part in
             part.alpha = 1
          }
      }
      
      @IBAction func evaluateButton(_ sender: Any) {
          // evaluate answers and score medal
          print("evaluated")
          var correctas = 0
          let activityName = "celulaBacterial"
          for button in options {
              if button.currentTitle! == terms[button.tag-1] {
                  print("esta es correcta")
                  button.setTitleColor(.green, for: .normal)
                  print(button.currentTitle!)
                  correctas += 1
              }else{
                  button.setTitleColor(.red, for: .normal)
            }
          }
          if correctas == 10 {
              print("actividad terminada")
              evalButton.isEnabled = false
              switch intentos {
              case 1:
                  if medalRepository!.assignMedal(type: "gold", activity: activityName) {
                      medal = "gold"
                      ARButton.enabled()
                      ARButton.setBackgroundImage(UIImage(named: "ARBacterial"), for: UIControl.State.normal)
                      performSegue(withIdentifier: "MedalModal", sender: self) 
                      completeCell()
                      MedalView.image = UIImage(named: "GoldMedal")
                  }
              case 2:
                  if medalRepository!.assignMedal(type: "silver", activity: activityName) {
                      medal = "silver"
                      ARButton.enabled()
                    ARButton.setBackgroundImage(UIImage(named: "ARBacteria"), for: UIControl.State.normal)
                      performSegue(withIdentifier: "MedalModal", sender: self)
                      completeCell()
                     MedalView.image = UIImage(named: "SilverMedal")
                  }
              case 3:
                  if medalRepository!.assignMedal(type: "bronze", activity: activityName) {
                    medal = "bronze"
                    ARButton.enabled()
                    ARButton.setBackgroundImage(UIImage(named: "ARBacteria"), for: UIControl.State.normal)
                      performSegue(withIdentifier: "MedalModal", sender: self)
                      completeCell()
                     MedalView.image = UIImage(named: "BronzeMedal")
                  }
              default:
                medal = "fail"
                performSegue(withIdentifier: "MedalModal", sender: self)
                MedalView.image = UIImage(named: "")
                ARButton.setBackgroundImage(UIImage(named: "ARBacterial"), for: UIControl.State.normal)
                  break;
              }
          } else {
              print("respuestas correctas: \(correctas), \(intentos) intentos")
              intentos += 1
              medal = "fail"
              performSegue(withIdentifier: "MedalModal", sender: Any.self)
          }
          
      }
      
      func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
          if let draggedButton = self.hasTextButton(location: session.location(in: self.view)) {
              originButton = draggedButton
              let buttonText = draggedButton.currentTitle!
              print(buttonText)
              let provider = NSItemProvider(object: NSString(string: buttonText))
              return [UIDragItem(itemProvider: provider)]
              
          }
          return []
      }
      
      func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
          if let previewButton = self.hasTextButton(location: session.location(in: self.view)) {
              return UITargetedDragPreview(view: previewButton)
          }
          return nil
      }
      
      func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
          return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeText as String]) && session.items.count == 1
      }
      
      func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
          return UIDropProposal(operation: .move)
      }
      
      func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
          session.loadObjects(ofClass: NSString.self) { dropItems in
              print("reached closure")
              let dropArray = dropItems as! [NSString]
              if let draggedText = dropArray.first! as String? {
                  self.buttonUpdate(location: session.location(in: self.view), newText: draggedText)
              }
              self.evalButtonCheck()
          }
      }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MedalModal") {
          let ac = segue.destination as! MedalViewController
          ac.medalType = medal
        }
    }
    
    
    
    
    
      @IBAction func ARview(_ sender: Any){
          let previewController = QLPreviewController()
          previewController.dataSource = self
          previewController.delegate = self
          present(previewController, animated: true)
      }
      func numberOfPreviewItems(in controller: QLPreviewController) -> Int { return 1 }
      
      func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
          let url = Bundle.main.url(forResource: "BactCell", withExtension: "usdz", subdirectory: "BactCell")!
          return url as QLPreviewItem
      }
      
      


}
