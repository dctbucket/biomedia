//
//  CaseActivity.swift
//  NOVUS
//
//  Created by DCT on 25/10/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit


class CaseActivityViewController: UIViewController, UIScrollViewDelegate{
    
    var scrollView: UIScrollView!
    var imageView: UIImageView!
    
    var scrolling = 0
    var screensize: CGRect = UIScreen.main.bounds
    var aplication = ""
    var imgHeight = 0
    var minheight = 0
    var sections = 0
    var average = 0
    var minAverage = 0
    
    
    @IBOutlet weak var ActionBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Config imageViewer
        switch aplication {
        case "vaccines":
            print("vaccines work")
            minheight = 6144
            imgHeight = 9000
            sections = 8
            average = imgHeight/sections
            minAverage = minheight/sections
            
        case "bacterias":
            print("bacterial work")
            minheight = 3840
            imgHeight = 5000
            sections = 5
            average = imgHeight/sections
            minAverage = minheight/sections
            
        case "mimetics":
            print("mimetics work")
            minheight = 5502
            imgHeight = 7994
            sections = 7
            average = imgHeight/sections
            minAverage = minheight/sections
            
            
        default:
            break;
        }
        
        
        
        imageView = UIImageView(image: UIImage(named: aplication))
        
        if screensize.width == 1366  {
            imageView.frame = CGRect(x:0, y:0, width: Int(screensize.width), height: imgHeight)
            imageView.contentMode = .scaleAspectFill
        }
        if screensize.width == 1024 {
            imageView.frame = CGRect(x:0, y:0, width: Int(screensize.width), height: minheight)
            imageView.contentMode = .scaleAspectFit
        }
        
        
        
        print(screensize)
        //Config the scrollview Navigation
        
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.contentSize = imageView.bounds.size
        scrollView.addSubview(imageView)
        
        //Adding the subview to mainView
        
        view.insertSubview(scrollView, at: 0)
    }
    
    
    
    
    
    func navigate(){
        print(scrolling)
        switch screensize.width {
        case 1366:
            scrolling += average
            print(scrolling)
            
            
            if scrolling == imgHeight {
                scrolling = 0
                ActionBtn.setImage(UIImage(named: "UpBtn"), for: .normal)
                scrollView.setContentOffset(CGPoint.init(x:0, y: scrolling), animated: true)
            } else {
                ActionBtn.setImage(UIImage(named: "DownBtn"), for: .normal)
                scrollView.setContentOffset(CGPoint.init(x:0, y: scrolling), animated: true)
            }
            
        case 1024:
            scrolling += minAverage
            print(scrolling)
            
            if  scrolling == minheight{
                scrolling = 0
                ActionBtn.setImage(UIImage(named: "UpBtn"), for: .normal)
                scrollView.setContentOffset(CGPoint.init(x:0, y: scrolling), animated: true)
            } else {
                ActionBtn.setImage(UIImage(named: "DownBtn"), for: .normal)
                scrollView.setContentOffset(CGPoint.init(x:0, y: scrolling), animated: true)
            }
            
        default:
            break;
        }
        
        
        
        
       
        
    }
    
    @IBAction func top(_ sender: Any) {
        navigate()
    }
    
    
    @IBAction func BackBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
