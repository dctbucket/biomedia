//
//  CaseMenuViewController.swift
//  NOVUS
//
//  Created by DCT on 31/10/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

class CaseMenuViewController: UIViewController  {
        
    var section = ""
    
    @IBOutlet var Menu: UIView!
    
    @IBOutlet var instructions: UIView!
    @IBOutlet weak var blurFx: UIVisualEffectView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var textInst: UITextView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var effect: UIVisualEffect!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.bg(Menu, name: "CasosBG")
        walktrougth()
    }
    
    func walktrougth() {
                effect = blurFx.effect
                
                self.view.addSubview(instructions)
                instructions.center = self.view.center
                //instructions.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                instructions.bringSubviewToFront(instructions)
                
                UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    self.instructions.transform = CGAffineTransform.identity
                    self.header.alpha = 1
                    self.textInst.alpha = 1
                    self.closeBtn.alpha = 1
                    self.blurFx.alpha = 1
            
                  })
            }
       func walkoff() {
                UIView.animate(withDuration: 0.5, animations: {
                    self.blurFx.effect = nil
                    
                }) {(success:Bool) in
                    self.instructions.removeFromSuperview()
                    self.blurFx.removeFromSuperview()
                }
            }
    
    
    @IBAction func closeBtn(_ sender: Any) {
        walkoff()
    }
    
    
    
    
    @IBAction func next(_ sender: UIButton) {
        section = sender.accessibilityLabel!
        performSegue(withIdentifier: "cases", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let ac = segue.destination as! CaseActivityViewController
        ac.aplication = section
    }
           
    @IBAction func back(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    
    
}
