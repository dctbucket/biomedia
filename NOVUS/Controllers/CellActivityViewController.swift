//
//  CellActivityViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/17/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import DeviceKit
import QuickLook


class CellActivityViewControlller: UIViewController, UIDragInteractionDelegate, UIDropInteractionDelegate, QLPreviewControllerDelegate, QLPreviewControllerDataSource {
    
    // MARK: - Properties
    
    var originButton: UIButton?
    var destinationButton: UIButton?
    
    @IBOutlet weak var evalButton: UIButton!
    @IBOutlet weak var ARButton: UIButton!
    @IBOutlet weak var intentLabel: UILabel!
    
    var medalRepository: RealSession?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var medal = ""
    var intentos: Int = 1
    var device = Device.current.description
    
    @IBOutlet weak var termTitle: UILabel!
    @IBOutlet weak var termDescription: UITextView!
    @IBOutlet var parts: [UIImageView]!
    @IBOutlet var options: [UIButton]!
    
    let terms: [String] = ["Núcleo", "Citoplasma", "ADN y Nucleolo", "Citoesqueleto", "M. Plasmática", "R.E.R", "Centriolos", "Mitocondria", "R.E.L", "Vesícula", "A. de Golgi", "Lisosoma", "Ribosoma"]
    
    let descriptions: [String: String] = ["Núcleo": "Guarda información codificada.", "Citoplasma": "Espacio en el que se encuentran los organelos.", "ADN y Nucleolo": "Dentro del Nucleolo, se encuentra el ADN, es la macromolécula que codifica información unida a las proteínas.", "Citoesqueleto": "Da estructura a la célula polimerizándose y despolimerizándose.", "Membrana plasmática": "Bicapa lipídica que permite aislar a la célula.", "Retículo endoplasmático rugoso": "Se realiza la síntesis de proteínas que pueden ser excretadas o de membranas.", "Centriolos": "En estos inicia el citoesqueleto, tienen una disposición de ángulo recto.", "Mitocondria": "Es la fábrica de energía." , "Retículo endoplasmático liso": "Tiene la función de síntesis de lípidos."  , "Vesícula": "En general se encarga de transportar sustancias.", "Aparato de Golgi": "Encargado de empaquetar y dirigir a las vesículas a su destino.", "Lisosoma": "Encargado de eliminar proteínas, usa proteasas. ", "Ribosoma": "Encargado de la producción de proteínas, tiene dos subunidades y está formado por ARNr."]
    
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImage.bg(view, name: "animalBG")
        
        let dragInteraction = UIDragInteraction(delegate: self)
        let dropInteraction = UIDropInteraction(delegate: self)
        dragInteraction.isEnabled = true
        
        view.addInteraction(dropInteraction)
        view.addInteraction(dragInteraction)
        
        ARButton.disabled()
        medalRepository = RealSession(context)
        
    }
    
    // MARK: - Activity Methods
    
    @IBAction func setDesc(_ sender: UIButton) {
        hint(sender)
    }
    
    func hint(_ sender: UIButton){
        for description in descriptions {
            if sender.accessibilityLabel ?? String() == description.key {
                termDescription.text = description.value
                termTitle.text = description.key
                
            }
        }
    }
    
    func getCellPart(senderTag: Int) -> UIImageView {
        var selectedPart = UIImageView()
        
        parts.forEach { part in
            if  part.tag == senderTag {
                selectedPart = part
            }
            part.alpha = 0.5
        }
        return selectedPart
    }
    
    @IBAction func optionSelect(sender: UIButton){
        getCellPart(senderTag: sender.tag).pulsate()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func evalButtonCheck() {
        var fail = false
        for number in 0..<options.count {
            let button = options[number]
            print(number)
            if button.currentTitle == nil || button.currentTitle == "" {
                print ("incomplete")
                fail = true
                break
            } else {
                print ("passes")
            }
        }
        if fail == false {
            evalButton.isEnabled = true
        }
    }
    
    // MARK: - Drag and Drop methods
    
    func hasTextButton(location: CGPoint) -> UIButton? {
        if let button = self.view.hitTest(location, with: nil) as? UIButton {
            if let title = button.currentTitle {
                if title == "" {
                    return nil
                }
                return button
            }
        }
        return nil
    }
    
    func buttonUpdate(location: CGPoint, newText: String) {
        if let button = self.view.hitTest(location, with: nil) as? UIButton {
            if button.currentTitle == nil || button.currentTitle == "" {
                print(originButton!.tag)
                if originButton!.tag > 0 {
                    originButton?.setTitle("", for: .normal)
                    print("persistent label")
                } else {
                    originButton?.removeFromSuperview()
                }
                button.setTitle(newText, for: .normal)
                button.setBackgroundImage(UIImage(named: "AnmBtn"), for: .normal)
            } else {
                print("texted label")
                let tempText = originButton!.titleLabel?.text
                originButton?.setTitle(button.titleLabel?.text, for: .normal)
                button.setTitle(tempText, for: .normal)
            }
            buttonAnswer(button)
        }
    }
    
    func buttonAnswer(_ button: UIButton) {
        if button.tag < 0 {
            //do nothing
        } else {
            print("tag is \(button.tag-1) with answer \(terms[button.tag-1])")
        }
    }
    
    func completeCell(){
        parts.forEach{part in
            part.alpha = 1
        }
    }
    
    //MARK: Evaluate Button
    
    @IBAction func evaluateButton(_ sender: Any) {
        // evaluate answers and score medal
        print("evaluated")
        var correctas = 0
        let activityName = "celulaAnimal"
        for button in options {
            if button.currentTitle! == terms[button.tag-1] {
                print("esta es correcta")
                print(button.currentTitle!)
                correctas += 1
            }
        }
        if correctas == 13 {
            print("actividad terminada")
            evalButton.isEnabled = false
            switch intentos {
            case 1:
                if medalRepository!.assignMedal(type: "gold", activity: activityName) {
                    medal = "ORO"
                    ARButton.enabled()
                    performSegue(withIdentifier: "MedalModal", sender: self)
                    completeCell()
                }
            case 2:
                if medalRepository!.assignMedal(type: "silver", activity: activityName) {
                    medal = "PLATA"
                    ARButton.enabled()
                    performSegue(withIdentifier: "MedalModal", sender: self)
                    completeCell()
                }
            case 3:
                if medalRepository!.assignMedal(type: "bronze", activity: activityName) {
                    ARButton.enabled()
                    medal = "BRONCE"
                    performSegue(withIdentifier: "MedalModal", sender: self)
                    completeCell()
                }
            default:
                break;
            }
        } else {
            print("respuestas correctas: \(correctas), \(intentos) intentos")
            intentLabel.text = "\(intentos)"
            intentos += 1
        }
        
    }
    
    
    
    func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
        if let draggedButton = self.hasTextButton(location: session.location(in: self.view)) {
            originButton = draggedButton
            let buttonText = draggedButton.currentTitle!
            print(buttonText)
            let provider = NSItemProvider(object: NSString(string: buttonText))
            return [UIDragItem(itemProvider: provider)]
        }
        return []
    }
    
    func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
        if let previewButton = self.hasTextButton(location: session.location(in: self.view)) {
            return UITargetedDragPreview(view: previewButton)
        }
        return nil
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
        return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeText as String]) && session.items.count == 1
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
        return UIDropProposal(operation: .move)
    }
    
    func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
        session.loadObjects(ofClass: NSString.self) { dropItems in
            print("reached closure")
            let dropArray = dropItems as! [NSString]
            if let draggedText = dropArray.first! as String? {
                self.buttonUpdate(location: session.location(in: self.view), newText: draggedText)
            }
            self.evalButtonCheck()
        }
    }
    
    // MARK: AR Button
    @IBAction func ARView(_ sender: Any) {
        let previewController = QLPreviewController()
        previewController.dataSource = self
        previewController.delegate = self
        present(previewController, animated: true)
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int { return 1 }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        let url = Bundle.main.url(forResource: "AnmCell", withExtension: "usdz", subdirectory: "AnmCell")!
        return url as QLPreviewItem
    }
    
    
}

