//
//  CellMenuViewController.swift
//  NOVUS
//
//  Created by DCT on 6/7/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit


class CellMenuViewController: UIViewController{
    
    var sessionControl: RealSession?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet var menuView: UIView!
    @IBOutlet var buttonOption: [UIButton]!
    
    @IBOutlet weak var ANmedal: UIImageView!
    @IBOutlet weak var VGmedal: UIImageView!
    @IBOutlet weak var BTmedal: UIImageView!
    
    var optionselected = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImage.bg(menuView, name: "CellBG")
    }
    
    @IBAction func optionSelect(_ sender: UIButton) {
        self.optionselected = sender.accessibilityLabel ?? String()
        performSegue(withIdentifier: optionselected, sender: self)
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
