//
//  DogmaMenuViewController.swift
//  NOVUS
//
//  Created by DCT on 6/7/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit
import AVKit

class DogmaMenuViewController: UIViewController {
   
    
    @IBOutlet weak var blurFx: UIVisualEffectView!
    @IBOutlet var instructions: UIView!
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var textInst: UITextView!
    @IBOutlet weak var closeBtn: UIButton!
    
    var opselect: [Question] = []
    var avPlayer: AVPlayer!
    let avPlayerController = AVPlayerViewController()
    let secondViewController = QuestionsViewController()
    var videOp: String!
    var sectionTitle = ""
    var effect: UIVisualEffect!
    
    let replicacion:[Question] =
        [Question(
            text: "¿En qué consiste el proceso de replicación?",
            answers: ["La síntesis de un gen",
                      "La síntesis de la copia del ADN de la célula",
                      "La generación de una proteína"],
            correctAnswer: "La síntesis de la copia del ADN de la célula" ),
         Question(
            text: "La mayoría de las biomoléculas que se encargan de que el proceso de replicación ocurra interviniendo en diferentes etapas son:",
            answers: ["Iniciadores",
                      "Fragmentos de Okazaki",
                      "Enzimas"],
            correctAnswer: "Enzimas"),
         Question(
            text: "La enzima ADN polimerasa tiene como función:",
            answers: ["Generar un iniciador",
                      "Separar las dos cadenas de ADN",
                      "Sintetiza la nueva cadena de ADN"],
            correctAnswer: "Sintetiza la nueva cadena de ADN"),
         Question(
            text: "La cadena que contiene los fragmentos de Okazaki es llamada:",
            answers: ["Rezagada",
                      "Continua",
                      "Inmadura"],
            correctAnswer: "Rezagada"),
         Question(
            text: "En el proceso de replicación, ¿qué se obtiene y a partir de qué?",
            answers: ["Una molécula de ARN a partir de ADN",
                      "Una copia completa del ADN de la célula a partir del mismo ADN",
                      "Una proteína a partir de un fragmento de ADN"],
            correctAnswer:  "Una copia completa del ADN de la célula a partir del mismo ADN"),

         ]
    
    let transcripcion:[Question] = [
          Question(
            text: "¿Cuál es el objetivo del proceso de trascripción en conjunto con la traducción?",
            answers:
                ["Generar más copias de ADN",
                "Generar proteínas útiles a la célula",
                "Generar moléculas de ARN"],
            correctAnswer: "Generar proteínas útiles a la célula"),
    
          Question(
            text: "¿Cuál es la enzima que se encarga de la síntesis de la molécula de ARN?",
            answers:
                ["ADN polimerasa",
                "Promotor y terminador",
                "ARN polimerasa"],
            correctAnswer: "ARN polimerasa"),
          
          Question(
            text: "¿Cuáles son las etapas de la transcripción?",
            answers:
                ["Iniciación, maduración y terminación",
                "Iniciación y terminación",
                "Iniciación, elongación y terminación"],
            correctAnswer: "Iniciación, elongación y terminación"),
          
          Question(
            text: "En el proceso de transcripción, ¿qué se obtiene y a partir de qué?",
            answers:
                ["ARN mensajero (ARNm) a partir de un fragmento de ADN",
                "Una copia completa del ADN de la célula a partir del mismo ADN",
                "Una proteína a partir de un fragmento de ADN"],
            correctAnswer: "ARN mensajero (ARNm) a partir de un fragmento de ADN"),
          ]
    
    
    let traduccion:[Question] = [
        Question(
            text: "¿Cuál es el objetivo del proceso traducción?",
            answers: [
                "Generar una secuencia de aminoácidos que dan lugar a una proteína",
                "Generar una secuencia de ARNm",
                "Sintetizar una copia de ADN" ],
        	correctAnswer: "Generar una secuencia de aminoácidos que dan lugar a una proteína"),
        
        Question(
            text: "¿Cuáles son las tres biomoléculas principales que participan en el proceso de traducción?",
            answers: [
                "ADN, ARNm y ribosoma",
                "ARNm, ARNt y ribosoma",
                "ADN, ARNm y ARNt"],
            correctAnswer: "ARNm, ARNt y ribosoma"),
        
        Question(
            text: "¿Cuál es la función del ARN de transferencia?",
            answers:
                ["Contener la secuencia específica de codones para fabricar una proteína determinada",
                "Reconocer el codón de terminación para provocar la separación del complejo",
                "Colocar a los aminoácidos en la secuencia apropiada haciendo uso de un anticodón que posee en su estructura"],
            correctAnswer: "Colocar a los aminoácidos en la secuencia apropiada haciendo uso de un anticodón que posee en su estructura"),
        
        Question(
            text: "En el proceso de traducción ¿qué se obtiene y a partir de qué?",
            answers: [
                "ARN mensajero (ARNm) a partir de un fragmento de ADN",
                "Una copia completa del ADN de la célula a partir del mismo ADN",
                "Una proteína a partir de ARNm"],
            correctAnswer: "Una proteína a partir de ARNm")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.bg(view, name: "DogmaBG")
        walktrougth()
    }
    // MARK: - Instructions
      
      func walktrougth() {
          effect = blurFx.effect
          
          self.view.addSubview(instructions)
          instructions.center = self.view.center
          //instructions.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
          instructions.bringSubviewToFront(instructions)
          
          UIView.animate(withDuration: 0.3, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
              self.instructions.transform = CGAffineTransform.identity
              self.header.alpha = 1
              self.textInst.alpha = 1
              self.closeBtn.alpha = 1
              self.blurFx.alpha = 1
            })
      }
     
      @IBAction func close(_ sender: Any) {
          walkoff()
      }
      
      func walkoff() {
          UIView.animate(withDuration: 0.5, animations: {
              self.blurFx.effect = nil
              
          }) {(success:Bool) in
              self.instructions.removeFromSuperview()
              self.blurFx.removeFromSuperview()
          }
      }
      
      

    @IBAction func selectOp(sender: UIButton){
        switch  sender.accessibilityLabel {
        case "Replicación":
            videOp = "/replicacion"
            playVideo()
            opselect = replicacion
            sectionTitle = "Replicación"
            
        case "Transcripción":
            videOp = "/transcripcion"
            playVideo()
            opselect = transcripcion
            sectionTitle = "Transcripción"
            
            
        case "Traducción":
            videOp = "/traduccion"
            playVideo()
            opselect = traduccion
            sectionTitle = "Traducción"
            
        default:
            break;
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       if segue.identifier == "questions"{
                let ac = segue.destination as! QuestionsViewController
                ac.questions = opselect
                ac.section = sectionTitle
        }
    }
    
    func playVideo() {
     
        // File Path location
        let filepath: String? = Bundle.main.path(forResource: videOp, ofType: "mp4")
        let fileURL = URL.init(fileURLWithPath: filepath!)
        
        //Video player fullscreen init
        avPlayer = AVPlayer(url: fileURL)
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        //Assign end of the video in an observer
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayerController.player?.currentItem)
        
        // Video Controls
        avPlayerController.showsPlaybackControls = true
        
        
        // Play video
        avPlayerController.player?.play()
        self.view.addSubview(avPlayerController.view)
        self.addChild(avPlayerController)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        performSegue(withIdentifier: "questions", sender: self)
    }

    @IBAction func backBtn(_ sender: Any) {
        performSegue(withIdentifier: "menu", sender: (Any).self)
    }
   
}

