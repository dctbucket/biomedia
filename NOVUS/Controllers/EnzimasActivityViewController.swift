//
//  EnzimasActivityViewController.swift
//  NOVUS
//
//  Created by DCT on 09/07/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit
import Foundation

class EnzimasActivityViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate {
    
    
    @IBOutlet weak var webBtn: UIButton!
    
    @IBOutlet weak var enzimaName: UILabel!
    @IBOutlet weak var evalBtn: UIButton!
    @IBOutlet weak var repBtn: UIButton!
    @IBOutlet weak var tradBtn: UIButton!
    @IBOutlet weak var tranBtn: UIButton!
    @IBOutlet weak var funcText: UITextView!
    @IBOutlet weak var enzimasImage: UIImageView!
    
    
    @IBOutlet weak var combotext1: UITextField!
    @IBOutlet weak var combotext2: UITextField!
    @IBOutlet weak var combotext3: UITextField!
    @IBOutlet weak var MedalHolder: UIImageView!
    
    
    var intents = 0
    
    var Answer = 0
    var enzName = ""
    var medal = ""
    
    var classData:[String] = []
    var weightData:[String] = []
    var nameData:[String] = []
    
    var correctAnswers:[String] = []
    var function = ""
    var processArray:[Bool] = []
    var link = ""

    @IBOutlet var MainView: UIView!
    var picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Carga el fondo de la vista
        backgroundImage.bg(view, name: "SubBg")
        
        picker.dataSource = self
        picker.delegate = self
        
        enzimasImage.image = UIImage(named: enzName)
        enzimaName.text = enzName
        combotext1.inputView = picker
        combotext2.inputView = picker
        combotext3.inputView = picker
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func webBtn(_ sender: Any) {
        performSegue(withIdentifier: "web", sender: self)
    }
    
    func evalSys() {
        Answer = 0
        //eval Combobox1
        if combotext1.text == correctAnswers[0]{
            combotext1.background = UIImage(named: "Correct")
            print("1st is Correct")
            Answer += 1
        } else {
            combotext1.background = UIImage(named: "Incorrect")
        }
        
        //eval Combobox2
        if combotext2.text == correctAnswers[1]{
            combotext2.background = UIImage(named: "Correct")
            print("2nd is correct")
            Answer += 1
        } else {
            combotext2.background = UIImage(named: "Incorrect")
        }
        
        //eval Combobox3
        if combotext3.text == correctAnswers[2]{
            combotext3.background = UIImage(named: "Correct")
            print("3rd is correct")
            Answer += 1
        }else{
            combotext3.background = UIImage(named: "Incorrect")
        }
        
        
        if Answer == 3 {
            switch intents {
            case 1:
                print("Gold medal")
                medal = "gold"
                MedalHolder.image = UIImage(named: "GoldMedal")
                funcText.text = function
                funcText.isHidden = processArray[0]
                repBtn.isHidden = processArray[1]
                tradBtn.isHidden = processArray[2]
                tranBtn.isHidden = processArray[3]
                      
            case 2:
                print("Silver medal")
                medal = "silver"
                MedalHolder.image = UIImage(named: "SilverMedal")
            case 3:
                medal = "bronze"
                print("Bronze medal")
                MedalHolder.image = UIImage(named: "BronzeMedal")
            
            default:
                MedalHolder.image = UIImage(named: "EmptyMedal")
                break;
            }
            
        } else {
            medal = "fail"
            print("fail works")
            performSegue(withIdentifier: "medals", sender: self)
            print(intents)
            
        }
        
        
        
    }
    
    @IBAction func evalBtn(_ sender: Any) {
        intents += 1
        evalSys()
        performSegue(withIdentifier: "medals", sender: self)
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if combotext1.isFirstResponder {
            return nameData.count
        }else if combotext2.isFirstResponder {
            return classData.count
        }else if combotext3.isFirstResponder {
            return weightData.count
        }
        return 0
    }
       
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent: Int) {
        if combotext1.isFirstResponder {
            combotext1.text = nameData[row]
            combotext2.isEnabled = true
            self.view.endEditing(true)
        } else if combotext2.isFirstResponder {
            combotext2.text = classData[row]
            combotext3.isEnabled = true
            self.view.endEditing(true)
        } else if combotext3.isFirstResponder {
            combotext3.text = weightData[row]
            evalBtn.isHidden = false
            self.view.endEditing(true)
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if combotext1.isFirstResponder {
            return nameData[row]
        }else if combotext2.isFirstResponder {
            return classData[row]
        }else if combotext3.isFirstResponder {
            return weightData[row]
        }
        return nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "web" {
            let ac = segue.destination as! UINavigationController
            let dc = ac.topViewController as! PDBWebViewController
            dc.pdblink = link
        } else if segue.identifier == "medals"{
            let mc = segue.destination as! MedalViewController
            mc.medalType = medal
        }
        
    }
    

}
