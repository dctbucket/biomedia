//
//  EnzimasViewControlller.swift
//  NOVUS
//
//  Created by DCT on 5/29/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

class EnzimasViewController: UIViewController{
    
    @IBOutlet weak var Btn1: UIButton!
    @IBOutlet weak var Btn2: UIButton!
    @IBOutlet weak var Btn3: UIButton!
    @IBOutlet weak var Btn4: UIButton!
    @IBOutlet weak var Btn5: UIButton!
    
    
    @IBOutlet var instructions: UIView!
    @IBOutlet weak var blurFx: UIVisualEffectView!
   
    @IBOutlet weak var header: UILabel!
    
    @IBOutlet var BtnCollection: [UIButton]!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var textInst: UITextView!
    @IBOutlet weak var closeBtn: UIButton!
    
    
    
    var effect: UIVisualEffect!
    struct enzima {
            var ID: String!
            var nameData : [String]!
            var classData : [String]!
            var weightData : [String]!
            var function : String!
            var correctAnswers :  [String]!
            var processArray : [Bool]!
            var link: String!
    }
    
    var ISUU = enzima.init(ID: "1SUU", nameData: ["Ribosoma 70S","ADN girasa A", "ARN polimerasa en complejo"], classData: ["Isomerasa", "ARN", "Ribosoma"], weightData: ["25 kDa", "2,193 kDa", "34 kDa"], function: "Es una enzima que reduce la tensión molecular originada por el superenrollamiento del ADN.", correctAnswers: ["ADN girasa A", "Isomerasa", "34 kDa"], processArray: [false, true, false, false], link: "https://www.rcsb.org/structure/1SUU")
    
    var ITAU = enzima.init(ID: "1TAU", nameData: ["TAQ Polimerasa", "ADN girasa A", "ARN polimerasa en complejo"], classData: ["ARN y ADN", "Transferasa", "ARN"], weightData: ["99 kDa", "25 kDa", "443 kDa"], function: "Es una enzima del grupo de las ADN polimerasas que se encarga de sintetizar, la cadena de ADN.", correctAnswers: ["TAQ Polimerasa", "Transferasa", "99 kDa"], processArray: [false, false, true, true], link: "https://www.rcsb.org/structure/1TAU")
    
    var AQSS = enzima.init(ID: "4Q5S", nameData: ["ADN girasa A", "ARN de transferencia", "ARN polimerasa en Complejo"], classData: ["Ribosoma", "Isomerasa","ARN y ADN"], weightData: ["443 kDa", "25 kDa", "34 kDa"], function: "Complejo de moléculas, incluyendo la enzima ARN polimerasa, que dan inicio al proceso de transcripción", correctAnswers: ["ARN polimerasa en Complejo", "ARN y ADN", "443 kDa"], processArray: [false, true, false, true], link: "https://www.rcsb.org/structure/4Q5S")
    
    var IEVV = enzima.init(ID: "1EVV", nameData: ["ARN de transferencia", "ADN girasa A", "ARN polimerasa en complejo"], classData: ["ARN", "Ribosoma", "ARN y ADN"], weightData: ["25 kDa", "2,193 kDa", "443 kDa"], function: "Molécula que transporta aminoácidos en la síntesis de las proteínas", correctAnswers: ["ARN de transferencia", "ARN", "25 kDa"], processArray: [false, true, true, false], link: "https://www.rcsb.org/structure/1EVV")
    
    var AVAQ = enzima.init(ID: "4V49", nameData: ["ADN girasa A", "ARN polimerasa en complejo", "Ribosoma 70S"], classData: ["Ribosoma", "Isomerasa", "ARN polimerasa en complejo"], weightData: ["443 kDa", "34 kDa", "2,193 kDa"], function: "Complejo de moléculas, que se encarga de la síntesis de proteínas", correctAnswers: ["Ribosomas 70S", "Ribosoma", "2,193 kDa"], processArray: [false, true, true, false], link: "https://www.rcsb.org/structure/4V49")
    
    var enzimas: [enzima] = []
    var enzimaSelected : enzima?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.bg(view, name: "EnzimasBG2")
        enzimas.append(ISUU)
        enzimas.append(ITAU)
        enzimas.append(AQSS)
        enzimas.append(IEVV)
        enzimas.append(AVAQ)
        walktrougth()
    }
    
    func walktrougth() {
             effect = blurFx.effect
             
             self.view.addSubview(instructions)
             instructions.center = self.view.center
             //instructions.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
             instructions.bringSubviewToFront(instructions)
             
             UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.instructions.transform = CGAffineTransform.identity
                self.header.alpha = 1
                self.textInst.alpha = 1
                self.closeBtn.alpha = 1
                self.blurFx.alpha = 1
               })
         }
    
    func walkoff() {
             UIView.animate(withDuration: 0.5, animations: {
                 self.blurFx.effect = nil
                 
             }) {(success:Bool) in
                 self.instructions.removeFromSuperview()
                 self.blurFx.removeFromSuperview()
             }
         }
    
    @IBAction func closeBtn(_ sender: Any) {
        walkoff()
    }
    
    
    @IBAction func enzimaSelect(_ sender: UIButton){
    
    for enzima in enzimas {
        if sender.accessibilityLabel ?? String() == enzima.ID {
            enzimaSelected = enzima
        }
    }
    
    performSegue(withIdentifier: "enzimas", sender: self)
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let Ec = segue.destination as! EnzimasActivityViewController
        
        Ec.enzName = enzimaSelected!.ID
        Ec.nameData =  enzimaSelected!.nameData
        Ec.classData = enzimaSelected!.classData
        Ec.weightData = enzimaSelected!.weightData
        Ec.correctAnswers = enzimaSelected!.correctAnswers
        Ec.function = enzimaSelected!.function
        Ec.processArray = enzimaSelected!.processArray
        Ec.link = enzimaSelected!.link
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
