//
//  InputViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/16/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

class InputViewController: UIViewController {
    
    
    @IBOutlet weak var IDIngress: UITextField!
    @IBOutlet weak var topgap: NSLayoutConstraint!
    @IBOutlet weak var guestBtn: UIButton!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var sessionControl: RealSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        backgroundImage.bg(view, name: "altHeadBG")
        sessionControl = RealSession(context)
    }

    @IBAction func GuestSession(_ sender: Any) {
        let text = "A00000000"
        sessionControl?.createSession(matricula: text)
        performSegue(withIdentifier: "LoginSegue", sender: nil)
        
    }
    
    @IBAction func loginButton(_ sender: Any) {
        let text = IDIngress.text!
        if text == "" || text.count < 7 {
            print("empty")
        } else {
            if ((sessionControl?.createSession(matricula: text))!) {
                performSegue(withIdentifier: "LoginSegue", sender: nil)
            }
        }
        
        
    }
    
    @IBAction func guestSession(_ sender: Any) {
        performSegue(withIdentifier: "guest", sender: nil)
    }
    
    
}
