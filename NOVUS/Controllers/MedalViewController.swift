//
//  MedalViewController.swift
//  NOVUS
//
//  Created by DCT on 15/07/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit

class MedalViewController: UIViewController {
    
    var medalType = " "
    
    
    @IBOutlet weak var Medal: UIImageView!
    @IBOutlet weak var medalBtn: UIButton!
    @IBOutlet weak var medalMessage: UITextView!
    @IBOutlet weak var medalTitle: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        switch medalType {
        case "gold":
            medalTitle.text = "¡Excelente!"
            medalMessage.text = "¡Felicidades! Completaste satisfactoriamente la actividad en tu primer intento."
            Medal.image = UIImage(named: "GoldMedal")
            medalBtn.setBackgroundImage(UIImage(named: "GoldBtn"), for: UIControl.State.normal)

        case "silver":
            medalTitle.text = "¡Muy bien!"
            medalMessage.text = "Completaste la actividad en tu segundo intento."
            Medal.image = UIImage(named: "SilverMedal")
            medalBtn.setBackgroundImage(UIImage(named: "SilverBtn"), for: UIControl.State.normal)
            
        case "bronze":
            medalTitle.text = "¡Bien hecho!"
            medalMessage.text = "Completaste la actividad en tu tercer intento."
            Medal.image = UIImage(named: "BronzeMedal")
            medalBtn.setBackgroundImage(UIImage(named: "BronceBtn"), for: UIControl.State.normal)
            
        case "try":
            medalTitle.text = "¡Felicidades!"
            medalMessage.text = "Haz concluido. Continua con el resto de las actividades."
            medalBtn.setBackgroundImage(UIImage(named: "SilverBtn"), for: UIControl.State.normal)
            
        case "fail":
            medalTitle.text = "Intenta de nuevo"
            medalMessage.text = "Revisa tus respuestas y responde nuevamente."
            Medal.image = UIImage(named: "EmptyMedal")
            medalBtn.setBackgroundImage(UIImage(named: "SilverBtn"), for: UIControl.State.normal)
            
        default:
            break;
        }
        print(medalType)
    }
    
    @IBAction func Success(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    


}
