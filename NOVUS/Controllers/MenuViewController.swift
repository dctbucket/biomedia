//
//  MenuViewController.swift
//  NOVUS
//
//  Created by DCT on 6/7/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

class MenuViewController: UIViewController {
    
    var sessionControl: RealSession?
    
    @IBOutlet var instructionsView: UIView!
    @IBOutlet weak var blurFx: UIVisualEffectView!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var effect:UIVisualEffect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set bacgound in the view
        backgroundImage.bg(view, name: "altBG2")
        sessionControl = RealSession(context)
        
        
        walktrougth()
    }
    
    func walktrougth() {
        effect = blurFx.effect
        self.view.addSubview(instructionsView)
        instructionsView.center = self.view.center
        
        instructionsView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        instructionsView.alpha = 0
        
        UIView.animate(withDuration: 0.5){
            self.blurFx.isHidden = false
            self.blurFx.alpha = 1
            self.instructionsView.alpha = 1
            self.instructionsView.transform = CGAffineTransform.identity
        }
        
    }
    
    func walkoff() {
        UIView.animate(withDuration: 0.3, animations: {
            self.instructionsView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.instructionsView.alpha = 0
            self.blurFx.effect = nil
            
        }) {(success:Bool) in
            self.instructionsView.removeFromSuperview()
            self.blurFx.removeFromSuperview()
        }
        
    }
    
    @IBAction func optionSelect(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            performSegue(withIdentifier: "CellActivity", sender: self)
        case 1:
            performSegue(withIdentifier: "DogmaActivity", sender: self)
        case 2:
            performSegue(withIdentifier: "EnzimasActivity", sender: self)
        case 3:
            performSegue(withIdentifier: "CaseActivity", sender: self)
        default:
            break;
        }
        
    }
    
    @IBAction func showinst(_ sender: Any) {
        
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        walkoff()
    }
    
}
