//
//  PDBWebViewController.swift
//  NOVUS
//
//  Created by DCT on 01/07/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit
import WebKit

class PDBWebViewController: UIViewController, WKNavigationDelegate {
    
    var pdblink = ""
    var webloader: WKWebView!
    
    override func loadView() {
        webloader = WKWebView()
        webloader.navigationDelegate = self
        view = webloader
     
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: pdblink)!
        webloader.load(URLRequest(url: url))
        
        print(pdblink)

    }
    
    // Close Web modal
    @IBAction func closeNav(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    

   
}
