//
//  QuestionsViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/27/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

struct Question {
    let text: String
    let answers: [String]
    let correctAnswer: String
    
}

class QuestionsViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var evalButton: UIButton!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var intentBtn: UIImageView!
    @IBOutlet weak var medalHolder: UIImageView!
    @IBOutlet weak var MedalView: UIImageView!
    
    var section: String = ""
    var slides:[QuizSlide] = []
    var questions:[Question] = []
    
    var medalRepository: RealSession?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var medal = ""
    var intentos: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        
        view.bringSubviewToFront(pageControl)
        view.bringSubviewToFront(evalButton)
        view.bringSubviewToFront(backBtn)
        view.bringSubviewToFront(intentBtn)
        view.bringSubviewToFront(header)
        view.bringSubviewToFront(medalHolder)
        // Do any additional setup after loading the view.
        
        backgroundImage.bg(view, name: "altBG2")
        header.text = section
        medalRepository = RealSession(context)
    }
    
    func moveControl() {
        pageControl.currentPage = 1
    }
    
    func createSlides() -> [QuizSlide] {
        var questionSlides: [QuizSlide] = []
        for q in questions {
            let slide = Bundle.main.loadNibNamed("QuizSlide", owner: self, options: nil)?.first as! QuizSlide
            slide.questionLabel.text = q.text
            slide.questionLabel.sizeToFit()
            
            for index in 0..<slide.answersButtons.count {
                slide.answersButtons[index].setTitle(q.answers[index], for: .normal)
                slide.answersButtons[index].addTarget(self, action: #selector(checkForCompletion), for: .touchUpInside)
            }
            questionSlides.append(slide)
        }
        return questionSlides
    }
    
    func setupSlideScrollView(slides: [QuizSlide]) {
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
    
    @objc func checkForCompletion(sender: Any) {
        print("checking for completion")
        var answered = 0
        for slide in slides {
            if let _ = slide.answersButtons[0].selected()?.titleLabel?.text {
                answered += 1
            }
        }
        if answered >= questions.count {
            evalButton.isEnabled = true
        }
    }
    
    @IBAction func quizEval(_ sender: Any) {
        var score = 0
        let activityName = "Replicación"
        for index in 0..<slides.count {
            let textView = slides[index].answersButtons[0].selected()?.titleLabel
            if let selectedAnswer = textView?.text {
                print("your answer is \(selectedAnswer) and the correct answer is \(questions[index].correctAnswer)")
                if selectedAnswer == questions[index].correctAnswer {
                    textView?.textColor = .green
                    score += 1
                }else {
                    textView?.textColor = .red
                }
            }
        }
        if score == slides.count {
            print("actividad terminada")
            evalButton.isEnabled = false
            switch intentos {
            case 1:
                if medalRepository!.assignMedal(type: "gold", activity: activityName) {
                    medal = "gold"
                    performSegue(withIdentifier: "MedalModal", sender: self)
                    MedalView.image = UIImage(named: "GoldMedal")
                }
            case 2:
                 if medalRepository!.assignMedal(type: "silver", activity: activityName) {
                     medal = "silver"
                     performSegue(withIdentifier: "MedalModal", sender: self)
                     MedalView.image = UIImage(named: "SilverMedal")
                }
            case 3:
                if medalRepository!.assignMedal(type: "bronze", activity: activityName) {
                     medal = "bronze"
                     performSegue(withIdentifier: "MedalModal", sender: self)
                     MedalView.image = UIImage(named: "BronzeMedal")
                }
            default:
                medal = "try"
                performSegue(withIdentifier: "MedalModal", sender: self)
                MedalView.image = UIImage(named: "")
                break;
            }
        } else {
            medal = "fail"
            performSegue(withIdentifier: "MedalModal", sender: Any.self)
            print("tu puntuación es: \(score), \(intentos) intentos")
            intentos += 1
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MedalModal"){
            let ac = segue.destination as! MedalViewController
            ac.medalType = medal
        }
        
    }
    
    func manualScroll() {
        if scrollView.contentSize.width == scrollView.contentOffset.x + view.frame.width {
            print("end")
        } else {
            scrollView.setContentOffset(CGPoint(x: scrollView.contentOffset.x + view.frame.width, y: 0), animated: true)
        }
    }
    
    @IBAction func dismissView(_ sender: Any) {
        performSegue(withIdentifier: "dismiss", sender: self)
        
    }
}




