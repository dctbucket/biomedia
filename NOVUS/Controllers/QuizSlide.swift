//
//  QuizSlide.swift
//  NOVUS
//
//  Created by DCT on 6/6/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit
import DLRadioButton

class QuizSlide: UIView {

    @IBOutlet weak var questionLabel: UITextView!
    @IBOutlet var answersButtons:  [DLRadioButton]!
    
    
}
