//
//  RulesViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/27/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit

class RulesViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var ready: UIButton!
    @IBOutlet weak var InstView: UIScrollView!
    
    var finalOption = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
 
    @IBAction func Selected(_ sender: Any) {
        performSegue(withIdentifier: finalOption, sender: self)
    }
    
}
