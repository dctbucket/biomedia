//
//  UserStatsViewController.swift
//  NOVUS
//
//  Created by DCT on 06/07/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit

class UserStatsViewController: UIViewController {
    
    
    var sessionControl: RealSession?
       
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var IDLabel: UILabel!
    @IBAction func closeBtn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.bg(view, name: "FormSheet")
        sessionControl = RealSession(context)
        IDLabel.text = sessionControl?.currentSession()?.matricula
        // Do any additional setup after loading the view.
    }
    @IBAction func logOut(_ sender: Any) {
        performSegue(withIdentifier: "LogOut", sender: self)
    }
    


}
