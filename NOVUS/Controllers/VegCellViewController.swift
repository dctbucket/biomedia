//
//  VegCellViewController.swift
//  NOVUS
//
//  Created by DCT on 07/11/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit
import MobileCoreServices
import Foundation
import QuickLook


class VegCellViewController: UIViewController, UIDragInteractionDelegate, UIDropInteractionDelegate, QLPreviewControllerDelegate, QLPreviewControllerDataSource {
    
    
 // MARK: - Properties
   var originButton: UIButton?
   var destinationButton: UIButton?
    
   var medalRepository: RealSession?
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var medal = ""
    var intentos: Int = 1
    
    

    @IBOutlet weak var evalButton: UIButton!
    @IBOutlet weak var ARButton: UIButton!
    @IBOutlet weak var termTitle: UILabel!
    @IBOutlet weak var termDescription: UITextView!
    @IBOutlet var parts: [UIImageView]!
    @IBOutlet var options: [UIButton]!
    @IBOutlet weak var MedalView: UIImageView!
    
    let terms: [String] = [ "Núcleo", "Citoplasma", "ADN y Nucléolo", "Citoesqueleto", "M. Plasmática", "R.E.R", "Cloroplasto", "Mitocondria", "R.E.L", "Gran vacuola", "Aparato de Golgi", "Pared celular", "Ribosoma"]
    
    let descriptions: [String: String] = ["Núcleo":"Guarda información codificada.", "Citoplasma": "Espacio en el que se encuentran los organelos.", "ADN y Nucléolo": "Dentro del Nucleolo se encuentra el ADN que es la macromolécula que codifica información unida a las proteínas.", "Citoesqueleto": "Da estructura a la célula polimerizándose y despolimerizándose", "Membrana plasmática": "Bicapa lipídica que permite aislar a la célula.", "Retículo endoplasmático rugoso": "Se realiza la síntesis de proteínas que pueden ser excretadas o de membranas.", "Cloroplasto": "Permite captar la luz y generar un carbohidrato.", "Mitocondria": "Es la fábrica de energía.", "Retículo endoplasmático liso": "Tiene la función de síntesis de lípidos.", "Gran vacuola": "Tienen un alto contenido de agua, almacenan residuos.", "Aparato de Golgi": "Encargado de empaquetar y dirigir a las vesículas a su destino.", "Pared celular": "Está constituida de celulosa y da estructura celular.", "Ribosoma": "Encargado de la producción de proteínas, tiene dos subunidades y está formado por ARNr."]

    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImage.bg(view, name: "vegetalBG")
        
        ARButton.disabled()
        
        medalRepository = RealSession(context)
        
        let dragInteraction = UIDragInteraction(delegate: self)
        let dropInteraction = UIDropInteraction(delegate: self)
        dragInteraction.isEnabled = true
        
        view.addInteraction(dropInteraction)
        view.addInteraction(dragInteraction)
        evalButton.isEnabled = false
                
    }
    
    @IBAction func setDesc(_ sender: UIButton){
           hintName(sender)
       }
    
    func hintName(_ sender: UIButton){
             for description in descriptions {
                 if sender.accessibilityLabel ?? String() == description.key {
                     termTitle.text = description.key
                     
                 }
                termTitle.isHidden = false
                termDescription.isHidden = true
             }
         }
       func hintDesc(_ sender: UIButton){
                for description in descriptions {
                    if sender.accessibilityLabel ?? String() == description.key {
                       termDescription.text = description.value
                        
                    }
                }
              termDescription.isHidden = false
              termTitle.isHidden = true
            }
          
       
       func getCellPart(senderTag: Int) -> UIImageView {
           var selectedPart = UIImageView()
           
           parts.forEach { part in
               if  part.tag == senderTag {
                   selectedPart = part
               }
               part.alpha = 0.5
           }
           return selectedPart
       }
       
       @IBAction func optionSelect(sender: UIButton){
        getCellPart(senderTag: sender.tag).pulsate()
        hintDesc(sender)
       }
       
       @IBAction func dismissView(_ sender: Any) {
           dismiss(animated: true, completion: nil)
       }
       

       func evalButtonCheck() {
           var fail = false
           for number in 0..<options.count {
               let button = options[number]
               print(number)
               if button.currentTitle == nil || button.currentTitle == "" {
                   print ("incomplete")
                   fail = true
                   break
               } else {
                   print ("passes")
               }
           }
           if fail == false {
               evalButton.isEnabled = true
           }
       }
       
       // MARK: - Drag and Drop methods
       
       func hasTextButton(location: CGPoint) -> UIButton? {
           if let button = self.view.hitTest(location, with: nil) as? UIButton {
               if let title = button.currentTitle {
                   if title == "" {
                       return nil
                   }
                   return button
               }
           }
           return nil
       }
       
       func buttonUpdate(location: CGPoint, newText: String) {
           if let button = self.view.hitTest(location, with: nil) as? UIButton {
               if button.currentTitle == nil || button.currentTitle == "" {
                   print(originButton!.tag)
                   if originButton!.tag > 0 {
                       originButton?.setTitle("", for: .normal)
                       originButton?.setBackgroundImage(UIImage(named: "Empty"), for: .normal)
                       originButton?.setTitle("", for: .normal)
                       print("persistent label")
                   } else {
                       originButton?.removeFromSuperview()
                   }
                   button.setTitle(newText, for: .normal)
                   button.setBackgroundImage(UIImage(named: "VegBtn"), for: .normal)
                   
               } else {
                   print("texted label")
                   let tempText = originButton!.titleLabel?.text
                   originButton?.setTitle(button.titleLabel?.text, for: .normal)
                   button.setTitle(tempText, for: .normal)
                   
               }
               buttonAnswer(button)
               
           }
       }
       
       func buttonAnswer(_ button: UIButton) {
           if button.tag < 0 {
               //do nothing
           } else {
               print("tag is \(button.tag-1) with answer \(terms[button.tag-1])")
           }
       }
       func completeCell(){
           parts.forEach{ part in
               part.alpha = 1
           }
       }
       //MARK:  Evaluate Button
    
    
       @IBAction func evaluateButton(_ sender: Any) {
           // evaluate answers and score medal
           print("evaluated")
           var correctas = 0
           let activityName = "celulaVegetal"
           for button in options {
               if button.currentTitle! == terms[button.tag-1] {
                   print("esta es correcta")
                   button.setTitleColor(.green, for: .normal)
                   print(button.currentTitle!)
                   correctas += 1
               } else{
                button.setTitleColor(.red, for: .normal)
                
            }
           }
        
           if correctas == 13 {
               ARButton.enabled()
               print("actividad terminada")
               evalButton.isEnabled = false
               switch intentos {
               case 1:
                   if medalRepository!.assignMedal(type: "gold", activity: activityName) {
                       medal = "gold"
                       ARButton.enabled()
                       ARButton.setBackgroundImage(UIImage(named: "ARVegetal"), for: UIControl.State.normal)
                       performSegue(withIdentifier: "MedalModal", sender: self)
                       completeCell()
                    MedalView.image = UIImage(named: "GoldMedal")
                   }
               case 2:
                   if medalRepository!.assignMedal(type: "silver", activity: activityName) {
                       medal = "silver"
                       ARButton.enabled()
                       ARButton.setBackgroundImage(UIImage(named: "ARVegetal"), for: UIControl.State.normal)
                       performSegue(withIdentifier: "MedalModal", sender: self)
                       completeCell()
                    MedalView.image = UIImage(named: "SilverMedal")
                   }
               case 3:
                   if medalRepository!.assignMedal(type: "bronze", activity: activityName) {
                       ARButton.enabled()
                       ARButton.setBackgroundImage(UIImage(named: "ARVegetal"), for: UIControl.State.normal)
                       medal = "bronze"
                       performSegue(withIdentifier: "MedalModal", sender: self)
                       completeCell()
                       MedalView.image = UIImage(named: "BronzeMedal")
                   }
               default:
                    medal = "try"
                    performSegue(withIdentifier: "MedalModal", sender: self)
                    MedalView.image = UIImage(named: "")
                    ARButton.setBackgroundImage(UIImage(named: "ARVegetal"), for: UIControl.State.normal)
                   break;
               }
           } else {
               print("respuestas correctas: \(correctas), \(intentos) intentos")
               intentos += 1
               medal = "fail"
               performSegue(withIdentifier: "MedalModal", sender: Any.self)
            
           }
           
       }
       
       func dragInteraction(_ interaction: UIDragInteraction, itemsForBeginning session: UIDragSession) -> [UIDragItem] {
           if let draggedButton = self.hasTextButton(location: session.location(in: self.view)) {
               originButton = draggedButton
               let buttonText = draggedButton.currentTitle!
               print(buttonText)
               let provider = NSItemProvider(object: NSString(string: buttonText))
               return [UIDragItem(itemProvider: provider)]
               
           }
           return []
       }
       
       func dragInteraction(_ interaction: UIDragInteraction, previewForLifting item: UIDragItem, session: UIDragSession) -> UITargetedDragPreview? {
           if let previewButton = self.hasTextButton(location: session.location(in: self.view)) {
               return UITargetedDragPreview(view: previewButton)
           }
           return nil
       }
       
       func dropInteraction(_ interaction: UIDropInteraction, canHandle session: UIDropSession) -> Bool {
           return session.hasItemsConforming(toTypeIdentifiers: [kUTTypeText as String]) && session.items.count == 1
       }
       
       func dropInteraction(_ interaction: UIDropInteraction, sessionDidUpdate session: UIDropSession) -> UIDropProposal {
           return UIDropProposal(operation: .move)
       }
       
       func dropInteraction(_ interaction: UIDropInteraction, performDrop session: UIDropSession) {
           session.loadObjects(ofClass: NSString.self) { dropItems in
               print("reached closure")
              
               let dropArray = dropItems as! [NSString]
               if let draggedText = dropArray.first! as String? {
                   self.buttonUpdate(location: session.location(in: self.view), newText: draggedText)
               }
               self.evalButtonCheck()
           }
       }
        
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "MedalModal") {
            let ac = segue.destination as! MedalViewController
            ac.medalType = medal
            
            
        }
        
    }
    
    
        
    
       //MARK: ARButton and Functions
       
       @IBAction func ARview(_ sender: Any) {
           let previewController = QLPreviewController()
           previewController.dataSource = self
           previewController.delegate = self
           present(previewController, animated: true)
       }
       
       func numberOfPreviewItems(in controller: QLPreviewController) -> Int { return 1 }
       
       func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
           let url = Bundle.main.url(forResource: "VegCell", withExtension: "usdz", subdirectory: "VegCell")!
           return url as QLPreviewItem
       }
       
       


}
