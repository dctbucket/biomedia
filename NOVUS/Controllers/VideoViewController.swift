//
//  VideoViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 5/2/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import UIKit
import AVKit


class VideoViewController: UIViewController {
    
    var avPlayer: AVPlayer!
    let avPlayerController = AVPlayerViewController()
    let secondViewController = QuestionsViewController()
    let videOp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playVideo()
    }
    
    func playVideo() {
        
        // File Path location
        let filepath: String? = Bundle.main.path(forResource: "/Videos/replicación", ofType: "mp4")
        let fileURL = URL.init(fileURLWithPath: filepath!)
        
        //Video player fullscreen init
        avPlayer = AVPlayer(url: fileURL)
        
        avPlayerController.player = avPlayer
        avPlayerController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        
        //Assign end of the video in an observer
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: avPlayerController.player?.currentItem)
        
        
        // Video Controls
        avPlayerController.showsPlaybackControls = false
        
        // Play video
        avPlayerController.player?.play()
        self.view.addSubview(avPlayerController.view)
        self.addChild(avPlayerController)
        
    }
    @objc func playerDidFinishPlaying(note: NSNotification){
        performSegue(withIdentifier: "question", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
}
