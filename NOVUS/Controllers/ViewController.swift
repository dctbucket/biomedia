//
//  ViewController.swift
//  NOVUS
//
//  Created by IDEA ITESM on 1/16/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var logo: UIImageView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        background.applymotionfx(for: background, magnitude: 0)
        logo.applymotionfx(for: logo, magnitude: -50)
    }
    
}

