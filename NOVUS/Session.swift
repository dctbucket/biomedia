//
//  Session.swift
//  NOVUS
//
//  Created by 88xdev on 7/12/19.
//  Copyright © 2019 IDEA ITESM. All rights reserved.
//

import Foundation
import CoreData

protocol SessionControl {
    
    func createSession(matricula: String) -> Bool
    
    func currentSession() -> Session?
    
    func logOut() -> Bool
    
    func getMedals() -> [String: String]
    
    func hasMedal(activity: String) -> Bool
    
    func assignMedal(type: String, activity: String) -> Bool
}

//struct MockSession: SessionControl {
//
//    func createSession(matricula: String) -> Bool {
//        let userName: String
//        if (matricula == "") {
//            userName = "debugUser"
//        } else {
//            userName = matricula
//        }
//        print("Create session for \(userName)")
//        return true
//    }
//
//    func currentSession() -> String {
//        return "debugSession"
//    }
//
//    func logOut() -> Bool {
//        print("Logged out")
//        return true
//    }
//
//    func getMedals() -> [String: String] {
//        return ["Activity1": "Bronze", "Activity2": "Silver", "Activity3": "Gold"]
//    }
//
//    func hasMedal(activity: String) -> Bool {
//        return false
//    }
//
//    func assignMedal(type: String, activity: String) -> Bool {
//        print("assigned \(type) medal for \(activity)")
//        return true
//    }
//
//}

struct RealSession: SessionControl {
    var context: NSManagedObjectContext
    
    init(_ context: NSManagedObjectContext) {
        self.context = context;
    }
    
    func createSession(matricula: String) -> Bool {
        let newSession = Session(context: context)
        newSession.matricula = matricula
        do {
            try context.save()
            UserDefaults.standard.set(matricula, forKey: "currentSession")
            return true
        } catch {
            print("Error saving context")
            return false
        }
    }
    
    func currentSession() -> Session? {
        
        //return session object
        let request: NSFetchRequest = Session.fetchRequest()
        let user = UserDefaults.standard.string(forKey: "currentSession") ?? ""
        let predicate = NSPredicate(format: "matricula MATCHES %@", user)
        request.predicate = predicate
        
        do {
            let sessione = try context.fetch(request)
            print(sessione)
            return sessione[0]
        } catch {
            print("Error in fetching")
            return nil
        }
    }
    
    func logOut() -> Bool {
        //try to logout from session, remove from userDefaults
        return true
    }
    
    func getMedals() -> [String: String] {
        let session = currentSession()!.matricula!
        let request: NSFetchRequest = Medal.fetchRequest()
        let sessionPredicate = NSPredicate(format: "medalSession.matricula MATCHES %@", session)
        request.predicate = sessionPredicate
        
        do {
            let medals = try context.fetch(request)
            print(medals)
            return [medals[0].activity!:medals[0].type!]
        } catch {
            print("Error in fetching")
            return ["Error": "error"]
        }
    }
    
    func hasMedal(activity: String) -> Bool {
        let session = currentSession()!.matricula!
        let request: NSFetchRequest = Medal.fetchRequest()
        let sessionPredicate = NSPredicate(format: "medalSession.matricula MATCHES %@", session)
        let activityPredicate = NSPredicate(format: "activity MATCHES %@", session)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [sessionPredicate, activityPredicate])
        
        do {
            let medal = try context.fetch(request)
            print(medal)
            return true
        } catch {
            return false
        }

    }
    
    func assignMedal(type: String, activity: String) -> Bool {
        let newMedal = Medal(context: context)
        newMedal.type = type
        newMedal.activity = activity
        newMedal.medalSession = currentSession()
        do {
            try context.save()
            print("assigned \(type) medal for \(activity)")
            return true
        } catch {
            print("Error saving context")
            return false
        }
    }
}
