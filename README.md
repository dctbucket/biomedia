# Biomedia  
  
Esta app fue desarrollada dentro del area de Innovación educativa del Tec de Monterrey en conjunto con los profesores de las materias de bioquímica del Tec de Monterrey Campus Puebla, la aplicación fue creada para los alumnos de primer ingreso con el fin de acercar y mostrar los temas de la materia desde un enfoque digital, utilizando herramientas de realidad aumentada.  
  
# Requerimientos  
  
Está app fue desarrollada bajo las licencias de desarrollo y publicación de [Apple](http://developer.apple.com/).

### Colaboración y desarrollo

Si quieres utilizar el código de este repo en el desarrollo de una app, puedes mandar un email a la siguiente dirección para solicitar los permisos de uso del repositorio.

dct.itesm@gmail.com


## Software  

Se requiere un equipo con Mac Os Catalina.


#### Xcode / IOS

Fue desarrollada dentro Xcode 11 utilizando multiples versiones de swift, está diseñada para funcionar en iPad en modo landscape, en cualquiera de sus modelos, a partir de la versión 10 de **IOs**.

### ARKit  
  
Para visualizar e interactuar con los recursos en realidad aumentada de *ARKit* es necesario contar con modelos de iPad compatibles:  
  
- iPad Pro de 12,9 pulgadas de 2015.  
- iPad Pro de 9,7 pulgadas de 2016.  
- iPad de 2017.  
- iPad Pro de 10,5 y 12,9 pulgadas de 2017.  
  
[Modelos compatibles de iPad](https://www.applesfera.com/ipad/todos-los-iphone-y-ipad-compatibles-con-arkit-la-realidad-aumentada-de-apple)  
  
### Créditos  
  
Los créditos de esta sección son por aquellas personas que se involucaron en la primera versión de esta app, como colaboradores del Tec de Monterrey.  
  
##### Profesores


##### Backend Development  
##### Frontend Development  
  
  
##### Digital ART  
##### Graphic design  
  
##### Instructional Design  
##### Admin and Management  

  